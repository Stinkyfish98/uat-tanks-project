﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDamage : MonoBehaviour
{
    
    [HideInInspector]
    public float damage;
    [HideInInspector]
    public GameObject parent;
    

	// Use this for initialization
	void Start ()
	{
	    parent = this.transform.parent.gameObject;
	    transform.parent = null;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<TankMotor>() != null)
        {
            other.GetComponent<TankData>().tankHealth -= damage;
            if (other.GetComponent<TankData>().tankHealth <= 0)
            {
                parent.GetComponent<TankData>().tankScore += other.GetComponent<TankData>().tankPoints;
            }
        }

        Destroy(gameObject);
    }
}
