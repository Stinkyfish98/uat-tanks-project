﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering;

public class InputController : MonoBehaviour
{
    [HideInInspector]
    public TankMotor motor;
    [HideInInspector]
    public TankData data;

    public enum InputScheme { WASD, arrowKeys };

    public InputScheme input = InputScheme.WASD;

    // Use this for initialization
	void Start ()
	{
	    motor = gameObject.GetComponent<TankMotor>();
	    data = gameObject.GetComponent<TankData>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    switch (input)
	    {
            case InputScheme.WASD:
                if (Input.GetKey(KeyCode.W))
                {
                    motor.Move(data.forwardMoveSpeed);
                }
                if (Input.GetKey(KeyCode.S))
                {
                    motor.Move(-data.backwardMoveSpeed);
                }
                if (Input.GetKey(KeyCode.D))
                {
                    motor.Rotate(data.turnSpeed);
                }
                if (Input.GetKey(KeyCode.A))
                {
                    motor.Rotate(-data.turnSpeed);
                }
                break;

            case InputScheme.arrowKeys:
	            if (Input.GetKey(KeyCode.UpArrow))
	            {
	                motor.Move(data.forwardMoveSpeed);
	            }
	            if (Input.GetKey(KeyCode.DownArrow))
	            {
	                motor.Move(-data.backwardMoveSpeed);
	            }
	            if (Input.GetKey(KeyCode.RightArrow))
	            {
	                motor.Rotate(data.turnSpeed);
	            }
	            if (Input.GetKey(KeyCode.LeftArrow))
	            {
	                motor.Rotate(-data.turnSpeed);
	            }
	            break;
	    }

	    if (Input.GetKey(KeyCode.Space))
	    {
	        if (data.canShoot == true)
	            StartCoroutine(motor.Fire(data.fireSpeed, data.bulletSpeed));
	    }
	}
}
