﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMotor : MonoBehaviour {

	private CharacterController characterController;
	private Transform tf;
    private TankData data;
    public GameObject bulletReference;
    private GameObject bullet;
    private Transform bulletSpawnPoint;

    private void Awake()
    {
        tf = gameObject.GetComponent<Transform>();
    }

    // Use this for initialization
    void Start () {
		characterController = gameObject.GetComponent<CharacterController> ();
	    data = gameObject.GetComponent<TankData>();
	    bulletSpawnPoint = transform.Find("BulletSpawnPoint");
	}
	
	// Update is called once per frame
	void Update () {
	    if (data.tankHealth <= 0)
	    {
	        Destroy(gameObject);
	    }
	}

	public void Move(float moveSpeed)
	{
		Vector3 speedVector;

		speedVector = tf.forward;

		speedVector *= moveSpeed;

		characterController.SimpleMove (speedVector);
	}

	public void Rotate(float turnSpeed)
	{

		Vector3 rotateVector;

		rotateVector = Vector3.up;

		rotateVector *= turnSpeed;

		rotateVector *= Time.deltaTime;

		tf.Rotate (rotateVector, Space.Self);

	}

    public IEnumerator Fire(float fireRate, float bulletSpeed)
    {
        bullet = Instantiate(bulletReference, bulletSpawnPoint.position, transform.rotation);

        bullet.transform.parent = gameObject.transform;

        Physics.IgnoreCollision(bullet.GetComponent<Collider>(), GetComponent<CharacterController>());

        bullet.GetComponent<Rigidbody>().AddForce(transform.forward * bulletSpeed);

        bullet.GetComponent<BulletDamage>().damage = data.bulletDamage;

        data.canShoot = false;

        yield return new WaitForSeconds(fireRate);

        data.canShoot = true;
    }

    public bool RotateTowards(Vector3 target, float speed)
    {
        Vector3 vectorToTarget;

        vectorToTarget = target - tf.position;

        Quaternion targetRotation = Quaternion.LookRotation(vectorToTarget);

<<<<<<< HEAD
        if(targetRotation == tf.rotation)
        {
            return false;
        }

        tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, data.turnSpeed * Time.deltaTime);
=======
        if (targetRotation == tf.rotation)
            return false;

        tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, speed * Time.deltaTime);
>>>>>>> master

        return true;
    }
}
