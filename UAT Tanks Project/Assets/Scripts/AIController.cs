﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{

    public enum AIState
    {
        //TODO: Implement Ambush State
        Chase,
        ChaseAndFire,
        CheckForFlee,
        Flee,
        Rest,
        Patrol
    };

    public AIState aiState = AIState.Chase;
    public float stateEnterTime;
    public GameObject player;
    public Transform target;
    private Transform tf;
    private int avoidanceStage = 0;
    public float avoidanceTime = 2.0f;
    public float fleeDistance = 1.0f;
    private float exitTime;
    public float aiSenseRadius;
    public float restingHealRate;
    private TankData data;
    private TankMotor motor;

	// Use this for initialization
	void Start ()
	{
	    data = gameObject.GetComponent<TankData>();
	    motor = gameObject.GetComponent<TankMotor>();
	    player = GameObject.FindGameObjectWithTag("Player");
	    target = player.transform;
	    tf = gameObject.transform;
	}
	
	// Update is called once per frame
	void Update () {
        // TODO: Change this to switch statement
        if (aiState == AIState.Chase)
        {
            // Perform Behaviors
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoChase();
            }

            // Check for Transitions
            if (data.tankHealth < data.maxHealth * 0.5f)
            {
                ChangeState(AIState.CheckForFlee);
            }
            else if (Vector3.SqrMagnitude(target.position - tf.position) <= (aiSenseRadius * aiSenseRadius))
            {
                ChangeState(AIState.ChaseAndFire);
            }
        }
        else if (aiState == AIState.ChaseAndFire)
        {
            // Perform Behaviors
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoChase();

                if (data.canShoot == true)
                    StartCoroutine(motor.Fire(data.fireSpeed, data.bulletSpeed));
            }
            // Check for Transitions
            if (data.tankHealth < data.maxHealth * 0.5f)
            {
                ChangeState(AIState.CheckForFlee);
            }
            else if (Vector3.SqrMagnitude(target.position - tf.position) > (aiSenseRadius * aiSenseRadius))
            {
                ChangeState(AIState.Chase);
            }
        }
        else if (aiState == AIState.Flee)
        {
            // Perform Behaviors
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoFlee();
            }

            // Check for Transitions
            if (Vector3.SqrMagnitude(target.position - tf.position) <= (aiSenseRadius * aiSenseRadius))
            {
                ChangeState(AIState.CheckForFlee);
            }
        }
        else if (aiState == AIState.CheckForFlee)
        {
            // Perform Behaviors
            CheckForFlee();

            // Check for Transitions
            if (Vector3.SqrMagnitude(target.position - tf.position) <= (aiSenseRadius * aiSenseRadius))
            {
                ChangeState(AIState.Flee);
            }
            else
            {
                ChangeState(AIState.Rest);
            }
        }
        else if (aiState == AIState.Rest)
        {
            // Perform Behaviors
            DoRest();

            // Check for Transitions
            if (Vector3.SqrMagnitude(target.position - tf.position) <= (aiSenseRadius * aiSenseRadius))
            {
                ChangeState(AIState.Flee);
            }
            else if (data.tankHealth >= data.maxHealth)
            {
                ChangeState(AIState.Chase);
            }
        }
        else if (aiState == AIState.Patrol)
        {
            DoPatrol();

            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoPatrol();
            }
            // Check for Transitions
            if (Vector3.SqrMagnitude(target.position - tf.position) > (aiSenseRadius * aiSenseRadius))
            {
                ChangeState(AIState.Chase);
            }
        }
    }

    public void CheckForFlee()
    {

    }

    public void DoRest()
    {
        data.tankHealth += restingHealRate * Time.deltaTime;
        data.tankHealth = Mathf.Min(data.tankHealth, data.maxHealth);
    }

    public void ChangeState(AIState newState)
    {
        aiState = newState;
        stateEnterTime = Time.time;
    }

    public void DoChase()
    {
        motor.RotateTowards(target.position, data.turnSpeed);

        if (CanMove(data.forwardMoveSpeed))
        {
            motor.Move(data.forwardMoveSpeed);
        }
        else
        {
            avoidanceStage = 1;
        }
    }

    public void DoAvoidance()
    {
        if (avoidanceStage == 1)
        {
            motor.Rotate(-1 * data.turnSpeed);

            if (CanMove(data.forwardMoveSpeed))
            {
                avoidanceStage = 2;

                exitTime = avoidanceTime;
            }
        }
        else if (avoidanceStage == 2)
        {
            if (CanMove(data.forwardMoveSpeed))
            {
                exitTime -= Time.deltaTime;
                motor.Move(data.forwardMoveSpeed);

                if (exitTime <= 0)
                    avoidanceStage = 0;
            }
            else
            {
                avoidanceStage = 1;
            }
        }
    }

    public void DoFlee()
    {
        Vector3 vectorToTarget = target.position - tf.position;

        Vector3 vectorAwayFromTarget = -1 * vectorToTarget;

        vectorAwayFromTarget.Normalize();

        vectorAwayFromTarget *= fleeDistance;

        Vector3 fleePosition = vectorAwayFromTarget + tf.position;
        motor.RotateTowards(fleePosition, data.turnSpeed);
        motor.Move(data.forwardMoveSpeed);
    }

    public bool CanMove(float speed)
    {
        RaycastHit hit;
        if (Physics.Raycast(tf.position, tf.forward, out hit, speed))
        {
            if(!hit.collider.CompareTag("Player"))
                return false;
        }

        return true;
    }

    public void DoPatrol()
    {

    }

}
