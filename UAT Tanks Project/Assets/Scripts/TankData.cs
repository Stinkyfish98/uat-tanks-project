﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour
{

    public float tankHealth = 100;
    public float maxHealth = 100;
	public float forwardMoveSpeed = 3;
    public float backwardMoveSpeed = 1;
	public float turnSpeed = 180;
    //TODO: Implement Mouse Aiming Mechanics
    [HideInInspector]
    public float turretTurnSpeed = 45;
    [HideInInspector]
    public float gunSpeed = 3;
    public float fireSpeed = 1;
    public float bulletSpeed = 150;
    public float bulletDamage = 35;
    public float tankPoints = 50;
    public float tankScore = 0;
    [HideInInspector]
    public bool canShoot = true;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
