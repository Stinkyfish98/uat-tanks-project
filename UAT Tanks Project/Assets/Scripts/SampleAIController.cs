﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleAIController : MonoBehaviour {

    public Transform[] waypoints;
    public TankMotor motor;
    public TankData data;
    private int currentWaypoint = 0;
    public float closeEnough = 1.0f;
    private Transform tf;
    public enum LoopType { Stop, Loop, PingPong };
    public LoopType loopType;

    private void Awake()
    {
        tf = gameObject.GetComponent<Transform>();
    }

    // Use this for initialization
    void Start () {
        motor = gameObject.GetComponent<TankMotor>();
        data = gameObject.GetComponent<TankData>();
    }
	
	// Update is called once per frame
	void Update () {
        if (motor.RotateTowards(waypoints[currentWaypoint].position, data.turnSpeed))
        {
        }
        else
        {
            motor.Move(data.forwardMoveSpeed);
        }

        if(Vector3.SqrMagnitude(waypoints[currentWaypoint].position - tf.position) < (closeEnough * closeEnough))
        {
            if (loopType == LoopType.Stop)
            {
                if (currentWaypoint < waypoints.Length - 1)
                {
                    currentWaypoint++;
                }
            }
            else if(loopType == LoopType.Loop)
            {
                if (currentWaypoint < waypoints.Length - 1)
                {
                    currentWaypoint++;
                }
                else
                {
                    currentWaypoint = 0;
                }
            }
        }
	}
}
